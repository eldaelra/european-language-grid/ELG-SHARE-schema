The **ELG (or ELG-SHARE) metadata schema** caters for the description of:

- **Language Resources and Technologies (LRTs)**, further classified into:

   - **tools & services**: *functional services* fully integrated and deployable in the ELG platform, but also downloadable *tools*, software in the form of source code, etc.,
   - **corpora**: collections of text documents, audio transcripts, audio and video recordings, etc.,
   - models & computational grammars, collectively referred to as **language descriptions**,
   - **lexical/conceptual resources**, which comprise computational lexica, gazetteers, ontologies, term lists, etc.
   
- related entities, i.e. 
    - activities and stakeholders from the wider area of Language Technology:
        - **projects** that have funded the development of LRTs or in which they have been deployed,
        - **organizations**, as well as **groups** and **persons** active in Language Technology in Europe
    - **licences and terms of use** associated with the LRTs
    - **documents** relevant to the LRTs.
 
Documentation is provided:

- [Metadata record (Base item)](https://european-language-grid.readthedocs.io/en/release1.0.0/Documentation/ELG-SHARE_xsd_Element_ms_LanguageResource.html)

- [Language Resource](https://european-language-grid.readthedocs.io/en/release1.0.0/Documentation/ELG-SHARE_xsd_Element_ms_LanguageResource.html)

	- [Tool/Service](https://www.european-language-grid.eu/wp-content/uploads/metadata/Documentation/ELG-SHARE_xsd_Element_ms_ToolService.html)

	- [Corpus](https://www.european-language-grid.eu/wp-content/uploads/metadata/Documentation/ELG-SHARE_xsd_Element_ms_Corpus.html)

	- [Language description](https://www.european-language-grid.eu/wp-content/uploads/metadata/Documentation/ELG-SHARE_xsd_Element_ms_LanguageDescription.html)

	- [Lexical/Conceptual resource](https://www.european-language-grid.eu/wp-content/uploads/metadata/Documentation/ELG-SHARE_xsd_Element_ms_LexicalConceptualResource.html)

- [Project](https://www.european-language-grid.eu/wp-content/uploads/metadata/Documentation/ELG-SHARE_xsd_Element_ms_Project.html)

- [Organization](https://www.european-language-grid.eu/wp-content/uploads/metadata/Documentation/ELG-SHARE_xsd_Element_ms_Organization.html)

- [Group](https://www.european-language-grid.eu/wp-content/uploads/metadata/Documentation/ELG-SHARE_xsd_Element_ms_Group.html)

- [Person](https://www.european-language-grid.eu/wp-content/uploads/metadata/Documentation/ELG-SHARE_xsd_Element_ms_Person.html)

- [Licence/Terms of use](https://www.european-language-grid.eu/wp-content/uploads/metadata/Documentation/ELG-SHARE_xsd_Element_ms_LicenceTerms_1.html)

- [Document](https://www.european-language-grid.eu/wp-content/uploads/metadata/Documentation/ELG-SHARE_xsd_Element_ms_Document.html)
