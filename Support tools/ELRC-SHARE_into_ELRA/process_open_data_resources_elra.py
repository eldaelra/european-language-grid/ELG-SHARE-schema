#!/usr/bin/env python
# coding: utf8

# ELDA SAS
# 9 rue des Cordelieres
# Paris 75013
# See the attached file for the license.

# This script converts the metadata elements from ELRC-SHARE metadata schema
# (derived from META-SHARE) into ELRA metadata (META-SHARE metadata 3.1).

import os
import xlrd
import argparse
import glob
import logging
try:
    import urlparse
except ImportError:
    from urllib import parse as urlparse
from xml.etree import ElementTree as ET

logging.basicConfig(
    level=logging.INFO,
    format='%(message)s'
)

logger = logging.getLogger(__name__)

NAMESPACES = {
    'elrc': 'http://www.elrc-share.eu/ELRC-SHARE_SCHEMA/v2.0/',
    'xsi': 'http://www.w3.org/2001/XMLSchema-instance',
    'schemaLocation': 'http://www.meta-share.org/META-SHARE_XMLSchema \
                       http://www.meta-share.org/META-SHARE_XMLSchema/v3.1/META-SHARE-Resource.xsd'
}

for k, v in NAMESPACES.items():
    ET.register_namespace(
        '', 'http://www.elrc-share.eu/ELRC-SHARE_SCHEMA/v2.0/'
    )


def update_validation_info(root):
    parent = ET.SubElement(root, 'validationInfo')
    if parent is not None:
        ET.SubElement(parent, 'validated').text = 'true'
        logger.info('ADD `validationInfo`')
    else:
        logger.warning('Block `validationInfo` is missing.')

    return


def update_identification_info(root, identifier, default_resource_view_url):
    """ Update block `identificationInfo` block for the new identifier and
    the new resource details view url.
    """
    parent = root.find('elrc:identificationInfo', NAMESPACES)
    if parent is not None:
        child = parent.find('elrc:identifier', NAMESPACES)
        if child is None:
            child = ET.SubElement(parent, 'identifier')
        child.text = identifier
        logger.info('SET `identifier` -> %s', identifier)

        child = parent.find('.//elrc:url', NAMESPACES)
        if child is None:
            child = ET.SubElement(parent, 'url')

        child.text = urlparse.urljoin(default_resource_view_url, identifier)
        logger.info('SET `url` -> %s', child.text)

    else:
        logger.warning('Block `identificationInfo` is missing.')

    return


def update_distribution_info(root):
    """ Update all blocks `distributionInfo` to change `availability` value from
    `underReview` to `available` if the value is present.
    """
    for distribution in root.findall('.//elrc:distributionInfo', NAMESPACES):
        availability = distribution.find('.//elrc:availability', NAMESPACES)
        if availability is not None:
            if availability.text == 'underReview':
                logger.error('CHANGE `availability` to `available`')
                availability.text = 'available'
    return


def update_text_format_info(root):
    """ Update block `textFormatInfo` for rename the field `dataFormat` to
    `mimeType`.
    """
    parent = root.find('.//elrc:textFormatInfo', NAMESPACES)
    if parent is not None:
        child = parent.find('.//elrc:dataFormat', NAMESPACES)
        if child is not None:
            logger.error('RENAME `dataFormat` -> `mimeType`')
            new_child = ET.SubElement(parent, 'mimeType')
            new_child.text = child.text
            parent.remove(child)
    return


def update_communication_info(root, default_email):
    """ Update all blocks `communicationInfo` to add a new element email if
    this one is missing with value from `default_email` argument.
    """
    for p in root.findall('.//elrc:communicationInfo', NAMESPACES):
        current_email_element = p.find('.//elrc:email', NAMESPACES)
        if current_email_element is None:
            new_email_element = ET.SubElement(p, 'email')
            new_email_element.text = default_email
            logger.error("ADD `email` -> %s", new_email_element.text)
    return


def identifiers_from_sheet(sheet, row):
    """ Return ELRA and ELRC identifers from cell 0 and cell 1 of
    `row` and `sheet` arguments.
    """
    return sheet.cell(row, 0).value, sheet.cell(row, 1).value


def main(input_path, workbook_xls_path,
         default_email, default_resource_view_url):
    workbook = xlrd.open_workbook(workbook_xls_path)
    for sheet in workbook.sheets():
        for row in range(1, sheet.nrows):
            elra_identifier, elrc_identifier = identifiers_from_sheet(
                sheet, row
            )

            resource_file = glob.glob(
                os.path.join(
                    input_path, '**/*{0}*.xml'.format(elrc_identifier)
                )
            )

            if len(resource_file) == 1:
                logger.info('UPDATE %s', resource_file[0])
                resource_file_xml = ET.parse(resource_file[0])
                root = resource_file_xml.getroot()

                update_validation_info(root)
                update_identification_info(
                    root, elra_identifier, default_resource_view_url
                )
                update_distribution_info(root)
                update_text_format_info(root)
                update_communication_info(root, default_email)
            elif len(resource_file) > 1:
                raise ValueError(
                    """
                    Multiple files with the same identifier ({identifier})
                    have been found.
                    """.format(elrc_identifier)
                )
            else:
                continue

            resource_file_xml.write(
                resource_file[0], xml_declaration=True, encoding='us-ascii'
            )

    return


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Convert ELRC resources XML to ELRA XML schema"
    )
    parser.add_argument("input_path", type=str, help="Input path directory")
    parser.add_argument("xls_file_path", type=str, help="File path of XLS")
    parser.add_argument(
        "--default-email",
        type=str,
        default="contact@elda.org",
        help="Default contact email for missing email fields"
    )
    parser.add_argument(
        "--default-resource-view-url",
        type=str,
        default="http://catalog.elra.info/en-us/repository/browse/",
        help="Base URL of resource details view suffixed with resource \
              identifier"
    )
    args = parser.parse_args()

    main(
        args.input_path, args.xls_file_path,
        args.default_email, args.default_resource_view_url
    )
