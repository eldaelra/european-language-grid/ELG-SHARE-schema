<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:ms="http://w3id.org/meta-share/meta-share/">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="@* | node()" name="identity-copy">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="//ms:Person | //ms:metadataCurator | //ms:metadataCreator">
		<xsl:variable name="surname">
			<xsl:value-of select="ms:surname"/>
		</xsl:variable>
		<xsl:variable name="givenName">
			<xsl:value-of select="ms:givenName"/>
		</xsl:variable>
		<xsl:copy>
			<xsl:copy-of select="ms:actorType"/>
			<xsl:copy-of select="ms:surname"/>
			<xsl:copy-of select="ms:givenName"/>
			<xsl:element name="ms:PersonalIdentifier">
				<xsl:attribute name="ms:PersonalIdentifierScheme"
					>http://w3id.org/meta-share/meta-share/elg</xsl:attribute>
				<xsl:value-of
					select="document('persons.xml')//person[@name = $givenName and @surname = $surname]/@elg_id"
				/>
			</xsl:element>
			<xsl:copy-of select="ms:email"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="//ms:Organization | //ms:affiliatedOrganization | //ms:coordinator | //ms:isDivisionOf | ms:participatingOrganization">
		<xsl:variable name="orgname">
			<xsl:value-of select="ms:organizationName"/>
		</xsl:variable>
		<xsl:copy>
			<xsl:copy-of select="ms:actorType"/>
			<xsl:copy-of select="ms:organizationName"/>
			<xsl:element name="ms:OrganizationIdentifier">
				<xsl:attribute name="ms:OrganizationIdentifierScheme"
					>http://w3id.org/meta-share/meta-share/elg</xsl:attribute>
				<xsl:value-of
					select="document('organizations.xml')//organization[@organization_name = $orgname]/@elg_id"
				/>
			</xsl:element>
			<xsl:copy-of select="ms:website"/>
		</xsl:copy>
	</xsl:template>
	
		<xsl:template match="//ms:Project | //ms:fundingProject | //ms:isFunderOf | //ms:isParticipatingOrganizationIn | //ms:isRelatedToProject | //ms:usageProject ">
		<xsl:variable name="projectName">
			<xsl:value-of select="ms:projectName"/>
		</xsl:variable>
		<xsl:copy>
			<xsl:copy-of select="ms:projectName"/>
			<xsl:copy-of select="ms:ProjectIdentifier"/>
			<xsl:element name="ms:ProjectIdentifier">
				<xsl:attribute name="ms:ProjectIdentifierScheme"
					>http://w3id.org/meta-share/meta-share/elg</xsl:attribute>
				<xsl:value-of
					select="document('projects.xml')//project[@project_name = $projectName]/@elg_id"
				/>
			</xsl:element>
			<xsl:copy-of select="ms:website"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="//ms:licenceTerms">
		<xsl:variable name="licenceName">
			<xsl:value-of select="ms:licenceTermsName"/>
		</xsl:variable>
		<xsl:copy>
			<xsl:copy-of select="ms:licenceTermsName"/>
			<xsl:copy-of select="ms:licenceTermsURL"/>
			<xsl:copy-of select="ms:LicenceIdentifier"/>
			<xsl:element name="ms:LicenceIdentifier">
				<xsl:attribute name="ms:LicenceIdentifierScheme"
					>http://w3id.org/meta-share/meta-share/elg</xsl:attribute>
				<xsl:value-of
					select="document('licences.xml')//licence[@licenceTermsName = $licenceName]/@elg_id"
				/>
			</xsl:element>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
