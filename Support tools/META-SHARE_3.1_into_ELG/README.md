## About this XSLT 

This converter has been developed under the umbrella of the work
carried out within the ELG initiative(https://www.european-language-grid.eu/).

This stylesheet works with the ELRA (META-SHARE 3.1) schema,
performing its analysis and transforming it to ELG-SHARE 1.0.2.

